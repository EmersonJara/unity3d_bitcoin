using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AjustContent : MonoBehaviour
{
    public float percentWith=1.0f;
    GridLayoutGroup layout;
    void Start()
    {
        layout = transform.GetComponent<GridLayoutGroup>();
        layout.cellSize= new Vector2(GetComponent<RectTransform>().rect.width * percentWith, layout.cellSize.y);
    }

    
}
