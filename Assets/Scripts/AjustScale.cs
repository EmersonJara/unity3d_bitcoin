using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasScaler))]
public class AjustScale : MonoBehaviour
{
    public CanvasScaler scaler;

    private void Start()
    {
        scaler = GetComponent<CanvasScaler>();
        scaler.referenceResolution = new Vector2(Screen.width, Screen.height);
        Debug.Log("Aplying "+ Screen.width+ " "+ Screen.height);
    }

}
