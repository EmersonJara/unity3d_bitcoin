using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class AnimationController : MonoBehaviour
{
    public List<AnimatedUI> animationList;

    private void Start()
    {
        foreach (var item in animationList)
        {
            var listener = item.element.gameObject.GetStateListener();
            listener.Enabled += () => item.doAnimation();

        }
    }

    [System.Serializable]
    public class AnimatedUI 
    {
        public RectTransform element;
        public AnimationType animType;
        public Ease animEase;
        public float animTime;
        public float delayTime;
        public Vector2 startVector2;
        public Vector2 endVector2;
        public Color startColor;
        public Color endColor;

        public void doAnimation() 
        {
            switch (animType)
            {
                case AnimationType.AnchoredPosition:
                    element.anchoredPosition = startVector2;
                    element.DOAnchorPos(endVector2, animTime).SetDelay(delayTime).SetEase(animEase);
                    break;
                case AnimationType.Color:
                    element.GetComponent<Image>().color = startColor;
                    element.GetComponent<Image>().DOColor(endColor, animTime).SetDelay(delayTime).SetEase(animEase);
                    break;
                case AnimationType.SizeDelta:
                    element.sizeDelta = startVector2;
                    element.DOSizeDelta(endVector2, animTime).SetDelay(delayTime).SetEase(animEase);
                    break;
                case AnimationType.Scale:
                    element.transform.localScale =  new Vector3(startVector2.x, startVector2.x, startVector2.x);
                    element.DOScale(new Vector3(endVector2.x, endVector2.x, endVector2.x), animTime).SetDelay(delayTime).SetEase(animEase);
                    break;
                case AnimationType.TextColor:
                    element.GetComponent<TextMeshProUGUI>().color = startColor;
                    element.GetComponent<TextMeshProUGUI>().DOColor(endColor, animTime).SetDelay(delayTime).SetEase(animEase);
                    break;
                default:
                    break;
            }
        }
    
    }

    public enum AnimationType 
    { 
        AnchoredPosition,
        Color,
        SizeDelta,
        Scale,
        TextColor
    }
}
