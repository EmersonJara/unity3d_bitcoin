using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Networking;

public class UIController : MonoBehaviour
{
    [Header("Buttons")]
    public Button StartBtn;
    public Button LoginBtn;
    public Button ForgotPassBtn;
    public Button CloseSignalBtn;
    public Button HomeBtn;
    public Button PositionsBtn;
    public Button InboxBtn;
    public Button NewsBtn;
    public Button ProfileBtn;
    public Button BackPostionsBtn;
    public Button BackProfileBtn;
    public Button BackNewsBtn;
    public Button BackInboxBtn;
    public Button SubmitRecoveryBtn;
    public Button BackToLoginBtn;
    public Button LinkBackToLoginBtn;
    public Button PrivacyPolicyBtn;
    public Button TermsAndConditionsBtn;
    public Button ContactUsBtn;
    public Button PrivacyPolicyBackToProfileBtn;
    public Button TermsAndContitionsBackToProfileBtn;
    public Button ContactUsBackToProfileBtn;
    public Button LogOutBtn;
    public Toggle RememberMeToogle;
    public Image RememberMeChecked;
    public Image RememberMeUnChecked;
    [Header("Inputs")]
    public TMP_InputField UserNameInputField;
    public TMP_InputField PasswordInputField;
    public TMP_InputField RecoveryInputField;
    public TMP_InputField NotifSignalInputField;
    [Header("Text")]
    public TextMeshProUGUI RecoveryMessage;
    public TextMeshProUGUI LoginLinkText;
    public TextMeshProUGUI RequestSignalText;
    [Header("DropDowns")]
    public TMP_Dropdown CurrencySignalDropDown;
    [Header("Backgrounds")]
    public RectTransform Background1;
    public RectTransform Background2;
    public RectTransform Background3;
    [Header("Panels")]
    public RectTransform MainPanel;
    public RectTransform LoginPanel;
    public RectTransform ForgotPanel;
    public RectTransform PositionsPanel;
    public RectTransform InboxPanel;
    public RectTransform NewsPanel;
    public RectTransform ProfilePanel;
    public RectTransform SignalsPanel;
    public RectTransform SignalTradeSpotPanel;
    public RectTransform FloatingMenuPanel;
    public RectTransform FloatingMessagePanel;
    public RectTransform LoadingPanel;
    public RectTransform PrivacyPolicyPanel;
    public RectTransform TermsAndConditionsPanel;
    public RectTransform ContactUsPanel;
    [Header("Prefabs")]
    public RectTransform SignalPrefab;
    public RectTransform NewPrefab;
    public RectTransform NotificationPrefab;
    public RectTransform TradeHistoryPrefab;
    [Header("Content")]
    public RectTransform ContentSignals;
    public RectTransform ContentNews;
    public RectTransform ContentNotifications;
    public RectTransform ContentTradeHistory;
    [Header("References")]
    public Sprite dollar;
    public Sprite coins;
    [Header("Controllers")]
    public LoginController logincontroller;

    private bool isRememberMe = true;
    private bool islogged = false;
    private List<GameObject> tradesgameObjects;
    private List<GameObject> signalsgameObjects;
    private List<GameObject> notificationsgameObjects;
    private List<GameObject> newsgameObjects;
    private void Start()
    {
        tradesgameObjects = new List<GameObject>();
        signalsgameObjects = new List<GameObject>();
        notificationsgameObjects = new List<GameObject>();
        newsgameObjects = new List<GameObject>();

        MainPanel.gameObject.SetActive(true);

        StartBtn.onClick.AddListener(StartAction);
        LoginBtn.onClick.AddListener(LoginAction);
        CloseSignalBtn.onClick.AddListener(CloseSignalDetailPanelAction);
        HomeBtn.onClick.AddListener(HomePanelAction);
        PositionsBtn.onClick.AddListener(PositionsPanelAction);
        InboxBtn.onClick.AddListener(InboxPanelAction);
        NewsBtn.onClick.AddListener(NewsPanelAction);
        ProfileBtn.onClick.AddListener(ProfilePanelAction);
        BackPostionsBtn.onClick.AddListener(BackToHomeAction);
        BackProfileBtn.onClick.AddListener(BackToHomeAction);
        BackNewsBtn.onClick.AddListener(BackToHomeAction);
        BackInboxBtn.onClick.AddListener(BackToHomeAction);
        ForgotPassBtn.onClick.AddListener(ForgotPanelAction);
        SubmitRecoveryBtn.onClick.AddListener(SubmitRecoveryAction);
        BackToLoginBtn.onClick.AddListener(BackToLoginAction);
        LinkBackToLoginBtn.onClick.AddListener(BackToLoginAction);
        TermsAndConditionsBtn.onClick.AddListener(OpenTermsAndConditionsAction);
        PrivacyPolicyBtn.onClick.AddListener(OpenPrivacyPolicyAction);
        ContactUsBtn.onClick.AddListener(OpenContactUsAction);
        PrivacyPolicyBackToProfileBtn.onClick.AddListener(BackToProfileAction);
        TermsAndContitionsBackToProfileBtn.onClick.AddListener(BackToProfileAction);
        ContactUsBackToProfileBtn.onClick.AddListener(BackToProfileAction);
        RememberMeToogle.onValueChanged.AddListener(OnRememberMeChanged);
        LogOutBtn.onClick.AddListener(LogOutAction);

        logincontroller.OnLoginResponse += LoginCallback;
        logincontroller.OnSignalsRetrieve += InstantiateSignals;
        logincontroller.OnNotificationsRetrieve += InstantiateNotifications;
        logincontroller.OnNewsRetrieve += InstantiateNews;
        logincontroller.OnSignalDetailRetrieve += ShowSignalDetail;
        logincontroller.OnTradeHistoryRetrieve += FillTradeHistory;

        if (PlayerPrefs.HasKey("remember")) 
        {
            isRememberMe= PlayerPrefs.GetInt("remember")==1?true:false;
            RememberMeToogle.isOn = isRememberMe;
        }

        if (PlayerPrefs.HasKey("logged"))
        {
            islogged = PlayerPrefs.GetInt("logged") == 1 ? true : false;
        }
    }

    private void FillTradeHistory()
    {
        clearList(tradesgameObjects);

        int c = 0;
        foreach (var item in logincontroller.trades)
        {
            GameObject go = Instantiate(TradeHistoryPrefab.gameObject, ContentTradeHistory.transform);
            TextMeshProUGUI first = go.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI second = go.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI third = go.transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI fourth = go.transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI fith = go.transform.GetChild(4).GetChild(0).GetComponent<TextMeshProUGUI>();
            Image background = go.GetComponent<Image>();
            Color newColor = new Color();
            ColorUtility.TryParseHtmlString("#EBF5F5", out newColor);
            if (c == 0 || c % 2 == 0)
            {
                background.color = new Color(1,1,1,0);
            }
            else 
            {
                background.color = newColor;
            }

            first.text = item.dated;
            second.text = item.symbol;
            third.text = item.status;
            fourth.text = item.price;
            fith.text = "empty";
            go.gameObject.SetActive(true);
            tradesgameObjects.Add(go);
            c++;
        }
    }

    private void clearList(List<GameObject>  list) 
    {
        for (int i = list.Count-1; i >= 0; i--)
        {
            Destroy(list[i]);
        }
        list.Clear();
    }

    private void LogOutAction()
    {
        islogged = false;
        PlayerPrefs.SetInt("logged", 0);
        PlayerPrefs.Save();
        ProfilePanel.gameObject.SetActive(false);
        FloatingMenuPanel.gameObject.SetActive(false);
        LoginPanel.gameObject.SetActive(true);
    }

    private void OnRememberMeChanged(bool arg0)
    {
        RememberMeUnChecked.gameObject.SetActive(!arg0);
        RememberMeChecked.gameObject.SetActive(arg0);
        if (arg0)
        {
            isRememberMe = true;
            PlayerPrefs.SetInt("remember", 1);
        }
        else 
        {
            isRememberMe = false;
            PlayerPrefs.SetInt("remember", 0);
            PlayerPrefs.DeleteKey("user");
            PlayerPrefs.DeleteKey("pass");
        }

        PlayerPrefs.Save();
    }

    private void OpenContactUsAction()
    {
        ProfilePanel.gameObject.SetActive(false);
        ContactUsPanel.gameObject.SetActive(true);        
    }

    private void OpenPrivacyPolicyAction()
    {
        ProfilePanel.gameObject.SetActive(false);
        PrivacyPolicyPanel.gameObject.SetActive(true);
    }

    private void OpenTermsAndConditionsAction()
    {
        ProfilePanel.gameObject.SetActive(false);
        TermsAndConditionsPanel.gameObject.SetActive(true);
    }

    private void BackToProfileAction()
    {
        PrivacyPolicyPanel.gameObject.SetActive(false);
        TermsAndConditionsPanel.gameObject.SetActive(false);
        ContactUsPanel.gameObject.SetActive(false);
        ProfilePanel.gameObject.SetActive(true);
    }

    private void LoginCallback(bool success)
    {
        LoadingPanel.gameObject.SetActive(false);
        if (success) 
        {           
            FloatingMenuPanel.gameObject.SetActive(true);
            SignalsPanel.gameObject.SetActive(true);
            islogged = true;
            PlayerPrefs.SetInt("logged", 1);
            PlayerPrefs.Save();
        }
        else 
        {
            LoginPanel.gameObject.SetActive(true);
            FloatingMessagePanel.gameObject.SetActive(true);
        }
    }

    private void SubmitRecoveryAction()
    {
        RecoveryInputField.gameObject.SetActive(false);
        SubmitRecoveryBtn.gameObject.SetActive(false);
        RecoveryMessage.gameObject.SetActive(true);
        BackToLoginBtn.gameObject.SetActive(true);
        LoginLinkText.gameObject.SetActive(true);        
    }

    private void BackToLoginAction()
    {
        ForgotPanel.gameObject.SetActive(false);
        LoginPanel.gameObject.SetActive(true);
    }

    private void ForgotPanelAction()
    {
        RecoveryInputField.gameObject.SetActive(true);
        SubmitRecoveryBtn.gameObject.SetActive(true);
        RecoveryMessage.gameObject.SetActive(false);
        BackToLoginBtn.gameObject.SetActive(false);
        LoginLinkText.gameObject.SetActive(false);

        ForgotPanel.gameObject.SetActive(true);
        LoginPanel.gameObject.SetActive(false);
    }

    private void InstantiateNotifications()
    {
        clearList(notificationsgameObjects);
        foreach (var item in logincontroller.notifications)
        {
            GameObject go = Instantiate(NotificationPrefab.gameObject, ContentNotifications.transform);
            TextMeshProUGUI first = go.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI second = go.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            first.text = item.notif;
            second.text = item.dated;
            go.SetActive(true);
            notificationsgameObjects.Add(go);
        }
    }

    private void InstantiateNews()
    {
        clearList(newsgameObjects);
        foreach (var item in logincontroller.news)
        {
            GameObject go = Instantiate(NewPrefab.gameObject, ContentNews.transform);
            TextMeshProUGUI first = go.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI second = go.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            RawImage image = go.transform.GetChild(2).GetComponent<RawImage>();
            Button button = go.transform.GetChild(3).GetComponent<Button>();
            first.text = item.title;
            second.text = UIController.UnixTimeStampToDateTime(item.published_on).ToString("MM/dd/yyyy h:mm tt");
            getTextureFromURL(item.imageurl, (texture) =>
            {
                image.texture = texture;
            });

            button.onClick.AddListener(() => {
                Application.OpenURL(item.url);
            });
            button.gameObject.AddComponent<RemoveListener>();
            go.SetActive(true);
            newsgameObjects.Add(go);
        }
    }

    private void getTextureFromURL(string url, Action<Texture2D> OnRetrieveTexture) 
    {
        UnityWebRequest uwr = UnityWebRequest.Get(url);
        DownloadHandlerTexture downloadtexture= new DownloadHandlerTexture(false);
        uwr.downloadHandler = downloadtexture;
        UnityWebRequestAsyncOperation async= uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                OnRetrieveTexture?.Invoke(downloadtexture.texture);
            }
        };

    }

    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
        // Unix timestamp is seconds past epoch
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        return dtDateTime;
    }

    private void BackToHomeAction()
    {
        DeactivatePanels();
        SignalsPanel.gameObject.SetActive(true);
    }

    private void ProfilePanelAction()
    {
        DeactivatePanels();
        ProfilePanel.gameObject.SetActive(true);
    }

    private void NewsPanelAction()
    {
        DeactivatePanels();
        NewsPanel.gameObject.SetActive(true);
    }

    private void InboxPanelAction()
    {
        DeactivatePanels();
        InboxPanel.gameObject.SetActive(true);
    }

    private void PositionsPanelAction()
    {
        DeactivatePanels();
        PositionsPanel.gameObject.SetActive(true);
    }

    private void HomePanelAction()
    {
        DeactivatePanels();
        SignalsPanel.gameObject.SetActive(true);
    }

    private void DeactivatePanels()
    {
        SignalsPanel.gameObject.SetActive(false);
        ProfilePanel.gameObject.SetActive(false);
        NewsPanel.gameObject.SetActive(false);
        InboxPanel.gameObject.SetActive(false);
        PositionsPanel.gameObject.SetActive(false);
        SignalTradeSpotPanel.gameObject.SetActive(false);
    }

    private void CloseSignalDetailPanelAction()
    {
        SignalTradeSpotPanel.gameObject.SetActive(false);
        SignalsPanel.gameObject.SetActive(true);
    }

    private void ShowSignalDetail()
    {
        NotifSignalInputField.text = logincontroller.currentSignal.notif;
        CurrencySignalDropDown.options = new List<TMP_Dropdown.OptionData>();
        CurrencySignalDropDown.options.Add(new TMP_Dropdown.OptionData(logincontroller.currentSignal.currency));
        CurrencySignalDropDown.options.Add(new TMP_Dropdown.OptionData(logincontroller.currentSignal.currency));
        CurrencySignalDropDown.value = 1;
        Debug.Log(logincontroller.currentSignal.request);
        RequestSignalText.text = "$" + logincontroller.currentSignal.request;
    }

    private void InstantiateSignals()
    {
        clearList(signalsgameObjects);
        foreach (var item in logincontroller.signals)
        {
            GameObject go = Instantiate(SignalPrefab.gameObject, ContentSignals.transform);
            TextMeshProUGUI first = go.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI second = go.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            Image third = go.transform.GetChild(2).GetChild(0).GetComponent<Image>();
            Button button = go.transform.GetChild(3).GetComponent<Button>();
            first.text = item.notif;
            second.text = item.dated;
            if (item.exchange == "spot")
            {
                Color newColor = new Color();
                ColorUtility.TryParseHtmlString("#4C85C7", out newColor);
                newColor.a = 0.1f;
                go.GetComponent<Image>().color = newColor;
            }
            else 
            {
                Color newColor = new Color();
                ColorUtility.TryParseHtmlString("#FEE4E4", out newColor);                
                go.GetComponent<Image>().color = newColor;
            }

            logincontroller.GetSignal(item.id,(signal)=> 
            {
                if (signal.type == "1")
                {
                    third.sprite = dollar;
                }
                else
                {
                    third.sprite = coins;
                }
            });

            button.onClick.AddListener(() => {
                SignalTradeSpotPanel.gameObject.SetActive(true);                
                logincontroller.GetSignal(item.id);
                SignalsPanel.gameObject.SetActive(false);
            });

            button.gameObject.AddComponent<RemoveListener>();
            signalsgameObjects.Add(go);
            go.gameObject.SetActive(true);
        }
    }

    private void LoginAction()
    {
        LoginPanel.gameObject.SetActive(false);
        if (isRememberMe)
        {
            PlayerPrefs.SetString("user", UserNameInputField.text);
            PlayerPrefs.SetString("pass", PasswordInputField.text);
            PlayerPrefs.Save();
        }
        logincontroller.Login(UserNameInputField.text, PasswordInputField.text);
        LoadingPanel.gameObject.SetActive(true);
    }

    private void StartAction()
    {
        MainPanel.gameObject.SetActive(false);
        Background1.gameObject.SetActive(false);
        Background2.gameObject.SetActive(true);
        if (PlayerPrefs.HasKey("user"))
        {
            UserNameInputField.text = PlayerPrefs.GetString("user");
            PasswordInputField.text = PlayerPrefs.GetString("pass");
            if (islogged)
            {
                logincontroller.Login(UserNameInputField.text, PasswordInputField.text);
            }
            else 
            {
                LoginPanel.gameObject.SetActive(true);
            }
        }
        else
        {           
            LoginPanel.gameObject.SetActive(true);           
        }
    }

    private void OnDestroy()
    {
        StartBtn.onClick.RemoveAllListeners();
        StartBtn.onClick.RemoveAllListeners();
    }
}
