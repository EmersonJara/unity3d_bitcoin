using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemoveListener : MonoBehaviour
{
    Button thisButton;
    void Start()
    {
        thisButton = GetComponent<Button>();
    }

    private void OnDestroy()
    {
        thisButton.onClick.RemoveAllListeners();
    }
}
