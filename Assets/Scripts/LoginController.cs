using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LoginController : MonoBehaviour
{
    public string token;
    public List<DataSignal> signals;    
    public List<DataNews> news;
    public List<Notification> notifications;
    public List<DataTradeHistory> trades;
    public SignalDetailData currentSignal;

    public event LoginResponseDelegate OnLoginResponse;
    public delegate void LoginResponseDelegate(bool success);
    public event SignalsRetrieve OnSignalsRetrieve;
    public event SignalDetailRetrieve OnSignalDetailRetrieve;
    public delegate void SignalsRetrieve();
    public delegate void SignalDetailRetrieve();
    public event NewsRetrieve OnNewsRetrieve;
    public delegate void NewsRetrieve();
    public event NotificationsRetrieve OnNotificationsRetrieve;
    public delegate void NotificationsRetrieve();
    public event TradeHistoryRetrieve OnTradeHistoryRetrieve;
    public delegate void TradeHistoryRetrieve();
    

    public void Login(string email, string password) 
    {
        string APILogin = "https://blockalpha.net/control/api/login?";
        UnityWebRequest uwr = UnityWebRequest.Post(APILogin + "email=" + email + "&password=" + password+ "&device_id=1&device_type=PC", "");
        UnityWebRequestAsyncOperation async= uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result!= UnityWebRequest.Result.ConnectionError) 
            {
                if (uwr.result.ToString() == "Success")
                {                   
                    LoginResponse response = JsonUtility.FromJson<LoginResponse>(uwr.downloadHandler.text);
                    token = response.data;
                    if (token != null)
                    {
                        OnLoginResponse?.Invoke(true);
                        //GetProfile();
                        //GetTradeHistory();
                        GetSignalsListing(0, 8);
                        GetNews();
                        GetNotifications();
                        GetTradeHistory();
                    }
                    else 
                    {
                        OnLoginResponse?.Invoke(false);
                        Debug.Log("Login Error");
                    }
                }
                else
                {
                    Debug.Log("Login Error");
                }
            }
        };
    }

    public void GetProfile() 
    {
        string APIProfile = "https://blockalpha.net/control/api/profile?";
        UnityWebRequest uwr = UnityWebRequest.Get(APIProfile + "token=" + token);
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                    Debug.Log(uwr.downloadHandler.text);
                    ProfileResponse response = JsonUtility.FromJson<ProfileResponse>(uwr.downloadHandler.text);
                    Debug.Log(response.data.name + " " + response.data.surname);
                }
            }
        };
    }

    public void GetCountryList()
    {
        string APICountry = "https://blockalpha.net/control/api/countries";
        UnityWebRequest uwr = UnityWebRequest.Get(APICountry);
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                    Debug.Log(uwr.downloadHandler.text);
                    CountryResponse response = JsonUtility.FromJson<CountryResponse>(uwr.downloadHandler.text);
                    foreach (var item in response.data)
                    {
                        Debug.Log(item);
                    }
                   
                }
            }
        };

    }

    public void GetTradeHistory() 
    {
        trades.Clear();
        string APITradeHistory = "https://blockalpha.net/control/api/fetch_trade_history?";
        UnityWebRequest uwr = UnityWebRequest.Get(APITradeHistory + "token=" + token+"&search=" +"&fromrow=0" + "&torow=100");
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                    TradeHistoryResponse response = JsonUtility.FromJson<TradeHistoryResponse>(uwr.downloadHandler.text);
                    foreach (var item in response.trades)
                    {
                        trades.Add(item);
                    }
                    OnTradeHistoryRetrieve?.Invoke();
                }
            }
        };
    }

    public void GetNews()
    {
        news.Clear();
        string APICountry = "https://blockalpha.net/control/api/fetch_news";
        UnityWebRequest uwr = UnityWebRequest.Get(APICountry);
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                    NewsResponse response = JsonUtility.FromJson<NewsResponse>(uwr.downloadHandler.text);
                    foreach (var item in response.data)
                    {
                        news.Add(item);
                    }
                    OnNewsRetrieve?.Invoke();
                }
            }
        };
    }

    public void GetNotifications()
    {
        notifications.Clear();
        string APICountry = "https://blockalpha.net/control/api/fetch_unseen_notifications?";
        UnityWebRequest uwr = UnityWebRequest.Get(APICountry+ "token=" + token);
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                    //Debug.Log(uwr.downloadHandler.text);
                    NotificationsResponse response = JsonUtility.FromJson<NotificationsResponse>(uwr.downloadHandler.text);
                    foreach (var item in response.data)
                    {
                        notifications.Add(item);
                    }
                    OnNotificationsRetrieve?.Invoke();
                }
            }
        };
    }

    public void GetSignalsListing(int start=0, int end=9999)
    {
        signals.Clear();
        string APISignals= "https://blockalpha.net/control/api/fetch_signals?";
        UnityWebRequest uwr = UnityWebRequest.Get(APISignals + "token=" + token+ "&fromrow="+start+ "&torow="+ end);
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                   // Debug.Log(uwr.downloadHandler.text);
                    Signals response = JsonUtility.FromJson<Signals>(uwr.downloadHandler.text);
                    foreach (var item in response.data)
                    {
                        signals.Add(item);
                        //Debug.Log(item.id);
                        //GetSignal(item.id);
                    }
                    OnSignalsRetrieve?.Invoke();
                }
                else {
                    Debug.Log("Error");
                }
            }
        };
    }

    public void GetSignal(string id, Action<SignalDetailData> OnSignalRetrieve=null)
    {
        string APISignals = "https://blockalpha.net/control/api/fetch_tradesignal_details?";
        UnityWebRequest uwr = UnityWebRequest.Get(APISignals + "token=" + token + "&signal_id=" + id);
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                    //Debug.Log(uwr.downloadHandler.text);
                    SignalDetail response = JsonUtility.FromJson<SignalDetail>(uwr.downloadHandler.text);
                    currentSignal = response.data;
                    OnSignalRetrieve?.Invoke(currentSignal);
                    if(OnSignalRetrieve == null)
                        OnSignalDetailRetrieve?.Invoke();
                }
            }
        };
    }

    public void GetTradeData(string symbol) {
        string APISignals = "https://blockalpha.net/control/api/fetch_tradedata?";
        UnityWebRequest uwr = UnityWebRequest.Get(APISignals + "token=" + token + "&symbol=" + symbol);
        UnityWebRequestAsyncOperation async = uwr.SendWebRequest();
        async.completed += (op) =>
        {
            if (uwr.result != UnityWebRequest.Result.ConnectionError)
            {
                if (uwr.result.ToString() == "Success")
                {
                    //Debug.Log(uwr.downloadHandler.text);
                    SignalDetail response = JsonUtility.FromJson<SignalDetail>(uwr.downloadHandler.text);
                    Debug.Log(response.data.description);
                }
            }
        }; 
    }

    [System.Serializable]
    public class LoginResponse 
    {
        public string status;
        public string message;
        public string data;    
    }

    [System.Serializable]
    public class ProfileResponse {
        public string status;
        public string message;
        public DataProfile data;
    }

    [System.Serializable]
    public class DataProfile {
        public string id;
        public string dated;
        public string name;
        public string surname;
        public string email;
        public string phone;
        public string profile_pic;
        public string account_details;
        public string govt_id;
        public string is_verified;
        public string bybit_key;
        public string bybit_secret;
        public string status;
        public string affiliate_id;
        public string gender;
        public string dob;
        public string country;
        public string current_exchange;
    }


    [System.Serializable]
    public class CountryResponse
    {
        public string status;
        public string message;
        public string[] data;
    }

    [System.Serializable]
    public class TradeHistoryResponse
    {
        public string status;
        public string message;
        public int recordsTotal;
        public int recordsFiltered;
        public DataTradeHistory[] trades;
    }

    [System.Serializable]
    public class DataTradeHistory
    {
        public string dated;
        public string symbol;
        public string status;
        public string price;
        public string pricewithusdt;
    }

    [System.Serializable]
    public class NewsResponse
    {
        public string status;
        public string message;
        public DataNews[] data;
    }

    [System.Serializable]
    public class DataNews
    {
        public string id;
        public string guid;
        public double published_on;
        public string imageurl;
        public string title;
        public string url;
        public string source;
        public string body;
        public string tags;
        public string categories;
        public string upvotes;
        public string downvotes;
        public string lang;
        public SourceInfoDataNews source_info;
    }

    [System.Serializable]
    public class SourceInfoDataNews
    {
        public string name;
        public string lang;
        public string img;
    }


    [System.Serializable]
    public class Signals
    {
        public string status;
        public string message;
        public DataSignal[] data;
    }

    [System.Serializable]
    public class DataSignal
    {
        public string id;
        public string dated;
        public string notif;
        public string for_admin;
        public string is_desktop;
        public string is_signal;
        public string description;
        public string exchange;
    }


    [System.Serializable]
    public class SignalDetail
    {
        public string status;
        public string message;
        public SignalDetailData data;
    }

    [System.Serializable]
    public class SignalDetailData
    {
        public string id;
        public string dated;
        public string notif;
        public string for_admin;
        public string is_desktop;
        public string is_signal;
        public string description;
        public string exchange;
        public string signal_txt;
        public string type;
        public string currency;
        public string request;
        public string stoploss;
        public string takeprofit;
    }

    [System.Serializable]
    public class NotificationsResponse
    {
        public int status;
        public string message;
        public int totalCount;
        public Notification[] data;
    }

    [System.Serializable]
    public class Notification
    {       
        public string id;
        public string notif;
        public string dated;
    }
}
